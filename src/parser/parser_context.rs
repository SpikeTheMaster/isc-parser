use std::collections::HashMap;

extern crate chrono;
use chrono::prelude::*;
use chrono::Duration;

use crate::parser::constants;
use crate::parser::events::Event;
use crate::parser::events::EventType;

pub struct ParseContext {
    name: String,
    properties: HashMap<String, String>,
    sub_contexts: Vec<ParseContext>,
}

impl ParseContext {
    pub fn new(name: String) -> Self {
        ParseContext {
            name,
            properties: HashMap::new(),
            sub_contexts: Vec::new(),
        }
    }

    pub fn parse(&mut self, tokens: &mut Vec<(&str, &str)>) {
        loop {
            let item = tokens.pop();
            if item == None {
                break;
            }

            let item = item.unwrap();

            if item.0 == constants::BEGIN {
                //println!("Found begin:{:?}", &item);
                let mut sub_context = ParseContext::new(item.1.to_string());
                sub_context
                    .properties
                    .insert(item.0.to_string(), item.1.trim().to_string());
                sub_context.parse(tokens);
                self.sub_contexts.push(sub_context);
            } else if item.0 == constants::END {
                //println!("Found End:{:?}", &item);

                self.properties
                    .insert(item.0.to_string(), item.1.trim().to_string());
                break;
            } else {
                //println!("Adding item:{:?}", &item);

                self.properties
                    .insert(item.0.to_string(), item.1.trim().to_string());
                if tokens.is_empty() {
                    break;
                }
            }
        }
    }

    pub fn extract_events(&self, events: &mut Vec<Event>) {
        for sub_context in self.sub_contexts.iter() {
            sub_context.extract_events(events);
            events.extend(match sub_context.properties.get(constants::BEGIN) {
                Some(event_type) if event_type == constants::VEVENT => Some(Event {
                    event_type: match sub_context.properties.get(constants::RRULE) {
                        Some(_) => EventType::RecurringEvent,
                        _ => EventType::NormalEvent,
                    },
                    properties: sub_context.properties.clone(),
                    start_date: match sub_context.properties.get(constants::DTSTART) {
                        Some(dt_start) => parse_isc_date(dt_start),
                        _ => {
                                println!("Event: {:?}", &sub_context);
                                panic!("Event had no start date!");
                            },
                    },
                    end_date: match sub_context.properties.get(constants::DTEND) {
                        Some(dt_end) => parse_isc_date(dt_end),
                        _ => {
                                //println!("Event: {:?}", &sub_context);
                                //println!("Event had no end date!");

                                // We don't have an end date, so guess the event ends a day later.
                                match sub_context.properties.get(constants::DTSTART) {
                                    Some(dt_start) => {
                                            parse_isc_date(dt_start) + Duration::days(1)
                                        },
                                    _ => {
                                            panic!("Event was missing an end date, and had no start date!");
                                        },
                                }
                            },
                    },
                }),
                _ => None,
            })
        }
    }
}
use std::fmt;
impl fmt::Debug for ParseContext {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Name:{}, Properties: {:?}, Subcontext: {:?}",
            self.name, self.properties, self.sub_contexts
        )
    }
}

fn parse_isc_date(date: &str) -> DateTime<Utc> {
    match date {
        date if (date.contains("TZID")) => {
            //TZID=Europe/London:20180817T121500
            //Just ignore the timezone, no easy way to parse that
            let date_parts: Vec<&str> = date.split(':').collect();
            //println!("1DateParts:{:?}", &date_parts);
            Utc.datetime_from_str(&date_parts[1], &"%Y%m%dT%H%M%S")
                .unwrap()
        }
        date if (date.contains("VALUE=DATE")) => {
            //DTSTART;VALUE=DATE:20180813
            let date_parts: Vec<&str> = date.split(':').collect();
            //println!("2DateParts:{:?}", &date_parts);
            Utc.datetime_from_str(
                &(date_parts[1].to_owned() + &"-00:00:00".to_owned()),
                "%Y%m%d-%H:%M:%S",
            )
            .unwrap()
        }
        //20180810T183000Z
        date => {
            //println!("3DateParts:{:?}", &date);
            Utc.datetime_from_str(date, &"%Y%m%dT%H%M%SZ")
                .unwrap()
                .with_timezone(&Utc)
        }
    }
}
