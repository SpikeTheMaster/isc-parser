pub const BEGIN: &str = "BEGIN";
pub const END: &str = "END";
pub const VEVENT: &str = "VEVENT";
pub const RRULE: &str = "RRULE";
pub const DTSTART: &str = "DTSTART";
pub const DTEND: &str = "DTEND";