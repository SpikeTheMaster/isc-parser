use std::collections::HashMap;
extern crate chrono;
use chrono::prelude::*;
use chrono::Duration;

use crate::parser::constants;

extern crate regex;
use regex::Regex;

#[derive(Debug, Clone)]
pub enum EventType {
    NormalEvent,
    RecurringEvent,
}

#[derive(Debug)]
pub struct RepetitionRule {
    pub frequency: String,
    //FREQ=YEARLY;BYMONTH=3;BYDAY=2SU
    //FREQ=YEARLY;BYMONTH=11;BYDAY=1SU
    //FREQ=MONTHLY;INTERVAL=3;BYMONTHDAY=29
    //FREQ=WEEKLY;WKST=MO;INTERVAL=4;BYDAY=SA
    //FREQ=WEEKLY;WKST=MO
    //FREQ=MONTHLY;UNTIL=20181019T225959Z;BYDAY=3FR
    //FREQ=MONTHLY;UNTIL=20120603;BYDAY=1SU"
    //FREQ=YEARLY
    //FREQ=WEEKLY;UNTIL=20150120T073000Z;INTERVAL=2;BYDAY=TU
    pub by_month: Option<u32>,
    pub by_month_day: Option<u32>,
    pub by_day: Option<String>,
    pub interval: Option<u32>,
    pub until: Option<DateTime<Utc>>,
}

#[derive(Debug, Clone)]
pub struct Event {
    pub event_type: EventType,
    pub properties: HashMap<String, String>,
    pub start_date: DateTime<Utc>,
    pub end_date: DateTime<Utc>,
}

impl Event {
    pub fn has_recurrence_between(
        &self,
        start_date: DateTime<Utc>,
        end_date: DateTime<Utc>,
    ) -> Option<Event> {
        lazy_static! {
            static ref FREQ: Regex = Regex::new(r"FREQ=(?P<F>[A-z]{1,})(;|$)").unwrap();
            static ref BYMONTH: Regex = Regex::new(r"BYMONTH=(?P<M>\d{1,})(;|$)").unwrap();
            static ref BYMONTHDAY: Regex = Regex::new(r"BYMONTHDAY=(?P<MD>\d{1,})(;|$)").unwrap();
            static ref BYDAY: Regex = Regex::new(r"BYDAY=(?P<D>[A-z0-9]{1,})(;|$)").unwrap();
            static ref INTERVAL: Regex = Regex::new(r"INTERVAL=(?P<I>\d{1,})(;|$)").unwrap();
            static ref UNTIL: Regex = Regex::new(r"UNTIL=(?P<U>\d{8}T\d{6}Z)(;|$)").unwrap();
            static ref UNTIL2: Regex = Regex::new(r"UNTIL=(?P<U>\d{8})(;|$)").unwrap();
            static ref COUNT: Regex = Regex::new(r"COUNT=(?P<C>\d{1,})(;|$)").unwrap();
        }

        match self.event_type {
            EventType::RecurringEvent => {
                let repetition_rule = self.properties.get(constants::RRULE).unwrap();
                //println!("reprule: {:?}", repetition_rule);

                let frequency = &FREQ.captures(repetition_rule).unwrap()["F"];
                let repetition_rule = RepetitionRule {
                    frequency: frequency.to_owned(),
                    by_month: if repetition_rule.contains("BYMONTH=") {
                        let caps = BYMONTH.captures(repetition_rule).unwrap();
                        Some(caps["M"].parse::<u32>().unwrap())
                    } else {
                        None
                    },
                    by_month_day: if repetition_rule.contains("BYMONTHDAY=") {
                        let caps = BYMONTHDAY.captures(repetition_rule).unwrap();
                        Some(caps["MD"].parse::<u32>().unwrap())
                    } else {
                        None
                    },
                    by_day: if repetition_rule.contains("BYDAY=") {
                        let caps = BYDAY.captures(repetition_rule).unwrap();
                        Some(caps["D"].to_owned())
                    } else {
                        None
                    },
                    interval: if repetition_rule.contains("INTERVAL=") {
                        let caps = INTERVAL.captures(repetition_rule).unwrap();
                        Some(caps["I"].parse::<u32>().unwrap())
                    } else {
                        None
                    },
                    until: if repetition_rule.contains("UNTIL") {
                        match UNTIL.captures(repetition_rule) {
                            Some(caps) => Some(
                                Utc.datetime_from_str(&caps["U"], &"%Y%m%dT%H%M%SZ")
                                    .unwrap()
                                    .to_owned(),
                            ),
                            None => {
                                let caps = UNTIL2.captures(repetition_rule).unwrap();
                                Some(
                                    Utc.datetime_from_str(
                                        &(caps["U"].to_owned() + &"-000000".to_owned()),
                                        &"%Y%m%d-%H%M%S",
                                    )
                                    .unwrap()
                                    .to_owned(),
                                )
                            }
                        }
                    } else if repetition_rule.contains("COUNT") {
                        match COUNT.captures(repetition_rule) {
                            Some(caps) => Some(
                                add_interval_to_date(caps["C"].parse::<u32>().unwrap(), &frequency, self.start_date)
                            ),
                            None => {
                                None
                            }
                        }
                    } else {
                        None
                    },
                };

                match repetition_rule.until {
                    Some(until) if until <= start_date => return None,
                    _ => (),
                }

                let mut event_start_dates: Vec<DateTime<Utc>> = Vec::new();
                let mut current_date = self.start_date;
                let interval = match repetition_rule.interval {
                    Some(interval) => interval,
                    None => 1,
                };
                while current_date <= end_date {
                    //println!("StartDate: {:?}, Date:{:?}", &start_date, &current_date);
                    //println!("Rep: {:?}", &repetition_rule);

                    current_date = add_interval_to_date(interval, &repetition_rule.frequency, current_date);
                    event_start_dates.push(current_date);
                }

                for event_start_date in event_start_dates.iter() {
                    if event_start_date >= &start_date && event_start_date <= &end_date {
                        let mut new_event = self.clone();
                        new_event.start_date = *event_start_date;
                        return Some(new_event); //Note we only allow for one occupance in the given time frame.
                    }
                }
                None
            }
            _ => panic!("This event type does not have occurrences"),
        }
    }
}

pub fn add_interval_to_date(interval: u32, frequency: &str, current_date: DateTime<Utc>) -> DateTime<Utc> {
    match frequency {
        "DAILY" => {
            current_date + Duration::days(i64::from(interval))
        }
        "WEEKLY" => {
            current_date + Duration::weeks(i64::from(interval))
        }
        "MONTHLY" => {
            let mut year = current_date.year();
            let mut month = current_date.month();

            if month + interval > 12 {
                year += 1;
                month = month + interval - 12;
            } else {
                month += interval;
            }

            let mut day = current_date.day();

            let days_in_target_month = if month == 12 {
                NaiveDate::from_ymd(year + 1, 1, 1)
            } else {
                NaiveDate::from_ymd(year, month + 1, 1)
            }
            .signed_duration_since(NaiveDate::from_ymd(year, month, 1))
            .num_days();

            if i64::from(day) > days_in_target_month {
                day = days_in_target_month as u32
            }
            //println!("Y: {:?}, M:{:?}, D:{:?}", &year, &month, &day);

            Utc.ymd(year, month, day).and_hms(
                current_date.hour(),
                current_date.minute(),
                current_date.second(),
            )
        }
        "YEARLY" => {
            Utc
                .ymd(
                    current_date.year() + interval as i32,
                    current_date.month(),
                    current_date.day(),
                )
                .and_hms(
                    current_date.hour(),
                    current_date.minute(),
                    current_date.second(),
                )
        }
        _ => panic!("Unknown repeat frequency: {}", frequency),
    }
}

pub fn create_kv_map(contents: &String) -> Box<Vec<(&str, &str)>> {
    Box::new(
        contents
            .split('\n')
            .filter_map(|line| {
                let kv: Vec<_> = line.splitn(2, |c| c == ';' || c == ':').take(2).collect();

                if kv.len() > 1 {
                    Some((kv[0], kv[1]))
                } else {
                    None
                }
            })
            .collect(),
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parser;
    use indoc::indoc;

    fn create_events(events: &mut Vec<Event>) {
        //This event should occur roughly at quarter ends on the 29th of the month

        //Create an event
        let mut parser = parser::parser_context::ParseContext::new("Root".to_string());
        let event_from_file = indoc!(
            "BEGIN:VCALENDAR
        BEGIN:VEVENT
        DTSTART;VALUE=DATE:20170929
        DTEND;VALUE=DATE:20170930
        RRULE:FREQ=MONTHLY;INTERVAL=3;BYMONTHDAY=29
        DTSTAMP:20190527T145547Z
        CREATED:20170916T142837Z
        SEQUENCE:0
        SUMMARY:this is a test quarterly event
        END:VEVENT
        END:VCALENDAR"
        )
        .to_string();

        let mut kv = create_kv_map(&event_from_file);
        //println!("Properties: {:?}", kv);
        kv.reverse();

        parser.parse(&mut kv);

        parser.extract_events(events);

        assert_eq!(events.len(), 1);
    }

    #[test]
    fn quarterly_repeating_events_repeats_for_first_quarter() {
        let mut events: Vec<Event> = Vec::new();
        create_events(&mut events);
        let event = &events[0];

        //2019-03-29
        let has_occurrence_1st_quarter_end = event.has_recurrence_between(
            Utc.ymd(2019, 3, 28).and_hms(0, 0, 0),
            Utc.ymd(2019, 3, 31).and_hms(0, 0, 0),
        );

        match has_occurrence_1st_quarter_end {
            Some(_) => (),
            _ => panic!("There should be an occurrence on first quarter end."),
        }
    }

    #[test]
    fn quarterly_repeating_events_repeats_for_second_quarter() {
        let mut events: Vec<Event> = Vec::new();
        create_events(&mut events);
        let event = &events[0];

        //2019-06-29
        let has_occurrence_2nd_quarter_end = event.has_recurrence_between(
            Utc.ymd(2019, 6, 28).and_hms(0, 0, 0),
            Utc.ymd(2019, 6, 30).and_hms(0, 0, 0),
        );

        match has_occurrence_2nd_quarter_end {
            Some(_) => (),
            _ => panic!("There should be an occurrence on second quarter end."),
        }
    }

    #[test]
    fn quarterly_repeating_events_repeats_for_third_quarter() {
        let mut events: Vec<Event> = Vec::new();
        create_events(&mut events);
        let event = &events[0];

        //2019-09-29
        let has_occurrence_3rd_quarter_end = event.has_recurrence_between(
            Utc.ymd(2019, 9, 28).and_hms(0, 0, 0),
            Utc.ymd(2019, 9, 30).and_hms(0, 0, 0),
        );

        match has_occurrence_3rd_quarter_end {
            Some(_) => (),
            _ => panic!("There should be an occurrence on third quarter end."),
        }
    }

    #[test]
    fn quarterly_repeating_events_repeats_for_forth_quarter() {
        let mut events: Vec<Event> = Vec::new();
        create_events(&mut events);
        let event = &events[0];

        //2019-12-29
        let has_occurrence_4th_quarter_end = event.has_recurrence_between(
            Utc.ymd(2019, 12, 28).and_hms(0, 0, 0),
            Utc.ymd(2019, 12, 31).and_hms(0, 0, 0),
        );

        match has_occurrence_4th_quarter_end {
            Some(_) => (),
            _ => panic!("There should be an occurrence on fourth quarter end."),
        }
    }

    #[test]
    fn quarterly_repeating_events_do_not_occur_at_non_quarter_end() {
        let mut events: Vec<Event> = Vec::new();
        create_events(&mut events);
        let event = &events[0];

        // It should not occur on 2019-05-29
        let has_occurrence_on_non_quarter_end = event.has_recurrence_between(
            Utc.ymd(2019, 5, 28).and_hms(0, 0, 0),
            Utc.ymd(2019, 5, 31).and_hms(0, 0, 0),
        );

        match has_occurrence_on_non_quarter_end {
            Some(e) => panic!("There should not be an occurrence!: {:?}", e),
            _ => (),
        }
    }
}
