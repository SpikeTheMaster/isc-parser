#![feature(test)]
extern crate test;
use test::Bencher;

use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

extern crate chrono;
use chrono::prelude::*;
use chrono::Duration;

mod parser;

use crate::parser::events;
use crate::parser::events::Event;
use crate::parser::events::EventType;
use crate::parser::parser_context::ParseContext;

extern crate clap;
use clap::{App, Arg};
use clap::value_t;

#[macro_use]
extern crate lazy_static;
extern crate reqwest;

fn format_day(t: u32) -> String {
    let suffix = match t {
        1 | 21 | 31 => "st".to_string(),
        2 | 22 => "nd".to_string(),
        3 | 23 => "rd".to_string(),
        _ => "th".to_string(),
    };

    t.to_string() + &suffix
}

fn main() {
    let matches = App::new("Calendar Feed Parser")
                          .version("1.1")
                          .author("Michael Wade <spikewade@gmail.com>")
                          .about("Outputs the next calendar item in the ics file(s)")
                          .arg(Arg::with_name("localfiles")
                            .short("l")
                            .takes_value(false)
                            .help("Specify to read from disk instead of url"))
                          .arg(Arg::with_name("urls")
                            .required(true)
                            .multiple(true)
                            .empty_values(false)
                            .help("URLs to the .ics files e.g. https://calendar.google.com/calendar/ical/XXXXXXX/basic.ics"))
                          .get_matches();

    let urls = matches.values_of("urls").unwrap().collect::<Vec<_>>();
    let local_files = matches.is_present("localfiles");

    let mut events: Vec<Event> = Vec::new();
    for url in urls.iter() {
        //println!("Url:{:?}", url);

        let cal_events =
            if local_files == true {
                process_isc(File::open(url).unwrap())
            } else {
                process_isc(reqwest::get(*url).unwrap())
            };

        for e in cal_events.iter() {
            events.push(e.to_owned());
        }
    }

    if !events.is_empty() {
        events.sort_by(|a, b| a.start_date.cmp(&b.start_date));
        let e = &events[0];
        println!(
            "{}{}\r\n{}",
            format_day(
                e.start_date
                    .format("%d")
                    .to_string()
                    .parse::<u32>()
                    .unwrap()
            ),
            e.start_date.with_timezone(&Local).format(" %H:%M"),
            e.properties.get("SUMMARY").unwrap()
        );
    }

    //println!("Next Events: {:?}", events);
}

fn process_isc(input: impl Read) -> Vec<Event> {
    let mut buf_reader = BufReader::new(input);
    let mut contents = String::new();
    buf_reader.read_to_string(&mut contents).unwrap();

    let mut map: Box<Vec<(&str, &str)>> = events::create_kv_map(&contents);

    map.reverse();
    let context = &mut ParseContext::new("Root".to_string());
    context.parse(&mut map);
    let mut events: Vec<Event> = Vec::new();

    context.extract_events(&mut events);
    //println!("Events:{:?}", &events);
    let now = Local::now();
    let two_days_time = now + Duration::hours(48);
    let start_of_day_in_2_days = Local
        .ymd(
            two_days_time.year(),
            two_days_time.month(),
            two_days_time.day(),
        )
        .and_hms(0, 0, 0);

    let events: Vec<Event> = events
        .iter()
        .filter_map(|event| match event.event_type {
            EventType::NormalEvent
                if event.start_date.with_timezone(&Local) >= now && event.start_date.with_timezone(&Local) <= start_of_day_in_2_days =>
            {
                Some(event.clone())
            }
            EventType::NormalEvent => None,
            EventType::RecurringEvent => {
                match event.has_recurrence_between(now.with_timezone(&Utc), start_of_day_in_2_days.with_timezone(&Utc)) {
                    Some(event) => Some(event.clone()),
                    None => None,
                }
            }
            //_ => panic!("Unrecognised event type: {:?}", event.event_type),
        })
        .collect();

    events
}

#[bench]
fn bench_cal_parser(b: &mut Bencher) {
    b.iter(|| {
        let isc_path = "C:\\Users\\Michael.Wade\\Downloads\\basic.ics";
        let file = File::open(isc_path).unwrap();

        // use `test::black_box` to prevent compiler optimizations from disregarding
        // unused values
        test::black_box(process_isc(file));
    });
}
